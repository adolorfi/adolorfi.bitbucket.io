var annotated_dup =
[
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "I2C_Driver", null, [
      [ "I2C_Driver", "classI2C__Driver_1_1I2C__Driver.html", "classI2C__Driver_1_1I2C__Driver" ]
    ] ],
    [ "motor_driver", null, [
      [ "DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "task_controller", "classtask__controller_1_1task__controller.html", "classtask__controller_1_1task__controller" ]
    ] ],
    [ "task_motor", null, [
      [ "task_motor", "classtask__motor_1_1task__motor.html", "classtask__motor_1_1task__motor" ]
    ] ],
    [ "task_TP", null, [
      [ "task_TP", "classtask__TP_1_1task__TP.html", "classtask__TP_1_1task__TP" ]
    ] ],
    [ "task_user", null, [
      [ "task_user", "classtask__user_1_1task__user.html", "classtask__user_1_1task__user" ]
    ] ],
    [ "TPdriver", null, [
      [ "TPdriver", "classTPdriver_1_1TPdriver.html", "classTPdriver_1_1TPdriver" ]
    ] ]
];