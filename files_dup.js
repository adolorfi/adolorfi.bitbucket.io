var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "I2C_Driver.py", "I2C__Driver_8py.html", [
      [ "I2C_Driver.I2C_Driver", "classI2C__Driver_1_1I2C__Driver.html", "classI2C__Driver_1_1I2C__Driver" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor_driver.py", "motor__driver_8py.html", [
      [ "motor_driver.DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "motor_driver.Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", "task__controller_8py" ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.task_motor", "classtask__motor_1_1task__motor.html", "classtask__motor_1_1task__motor" ]
    ] ],
    [ "task_TP.py", "task__TP_8py.html", "task__TP_8py" ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.task_user", "classtask__user_1_1task__user.html", "classtask__user_1_1task__user" ]
    ] ],
    [ "TPdriver.py", "TPdriver_8py.html", [
      [ "TPdriver.TPdriver", "classTPdriver_1_1TPdriver.html", "classTPdriver_1_1TPdriver" ]
    ] ]
];